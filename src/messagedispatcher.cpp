//
// Created by user on 6/22/19.
//

#include "messagedispatcher.hpp"

boost::signals2::signal<void(std::shared_ptr<ClientContext>,
                             uint8_t tag,
                             const json &data)
> &MessageDispatcher::getSignal(const std::string &name) {
    return mDispatchSignals[name];
}


void MessageDispatcher::processMsg(std::shared_ptr<ClientContext> ctx, uint8_t tag, DataWrapper data) {
    mThreadPool.queueFn(std::bind(&MessageDispatcher::dispatchDoc, this, ctx, tag, data));
}

void MessageDispatcher::dispatchDoc(std::shared_ptr<ClientContext> ctx, uint8_t tag, DataWrapper data) {
    try {
        json j = data.getJSON();

        if (j["message-type"] != "action") {
            return;
        }

        auto it = mDispatchSignals.find(j["action"]);
        if (it == mDispatchSignals.end()) {
            return;
        }

        it->second(ctx, tag, j);
    } catch (json::exception &e) {
        json j = {{"message-type" , "response"},
                  {"status", "error"},
                  {"error-msg", e.what()}};
        ctx->send(j, tag);
    }
    /*
    rapidjson::Document doc = data.getDoc();
    if (doc.HasParseError()) return;


    // check that the message type is an action
    if (!doc.HasMember("message-type")) return;
    if (doc["message-type"] != "action") return;

    // find the action
    auto act = doc.FindMember("action");
    if (act == doc.MemberEnd()) return;


    auto it = mDispatchSignals.find(act->value.GetString());
    if (it == mDispatchSignals.end()) return;

    it->second(ctx, tag, doc);
     */
}

MessageDispatcher::MessageDispatcher(ThreadPool &pool) :
    mThreadPool(pool)
{

}
