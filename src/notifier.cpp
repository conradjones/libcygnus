//
// Created by user on 12/28/19.
//

#include "notifier.hpp"

Notifier::Notifier() :
    notify_val_(false)
{

}

void Notifier::wait() {
    std::unique_lock lock(notify_mtx_);
    notify_cv_.wait(lock, [&](){return notify_val_;});
    notify_val_ = false;
}

void Notifier::notify() {
    {
        auto guard = std::lock_guard(notify_mtx_);
        notify_val_ = true;
    }
    notify_cv_.notify_all();

}
