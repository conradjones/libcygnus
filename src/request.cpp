//
// Created by user on 6/21/19.
//

#include "request.hpp"

#include "clientcontext.hpp"

Request::Request(DataWrapper data) :
    mReqData(data),
    mCTX(nullptr)
{

}

Request::~Request() {
    if (mCTX != nullptr) {
        mCTX->cancelRequest(*this);
    }
}

DataWrapper Request::getResponse() {
    auto future = mResponse.get_future();
    future.wait();
    return future.get();
}
