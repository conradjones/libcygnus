//
// Created by user on 6/21/19.
//

#include "clientcontext.hpp"
#include <unistd.h>
#include <sys/socket.h>

#include "dataheader.hpp"

ClientContext::ClientContext(ConnectionManager &manager) :
    mConnectionManager(manager)
{
    // populate tag stack
    for (uint8_t i = 1; i < 255; ++i) {
        mAvailableTags.push(i);
    }

    for (auto& i : mRequestMap) {
        i = nullptr;
    }
}

bool ClientContext::processRequest(Request &request) {
    // find unused tag
    // TODO: ensure graceful failure if tag > 255
    mTagLock.lock();
    uint8_t tag = 1;
    while (tag < 255 && mRequestMap[tag - 1] != nullptr) {
        ++tag;
    }

    mRequestMap[tag - 1] = &request;
    mTagLock.unlock();
    request.mCTX = this;

    return send(request.mReqData.getData(), request.mReqData.size(), tag);
}

bool ClientContext::send(void *data, size_t length, uint8_t tag) {
    if (!mOpen) return false;

    // create data header
    DataHeader dh {.len = length, .tag = tag};

    // acquire lock for the FD
    mFDLock.lock();

    // send the header
    ::send(mFD, &dh, sizeof(dh), 0);

    // send the message
    size_t bytesSent = 0;
    while (bytesSent < length){
        bytesSent += ::send(mFD, data, length, 0);

    }

    // release
    mFDLock.unlock();

    return true;
}

bool ClientContext::send(DataWrapper dw, uint8_t tag) {
    return send(dw.getData(), dw.size(), tag);
}

bool ClientContext::isOpen() const {
    return mOpen;
}

bool ClientContext::cancelRequest(Request &request) {
    for (auto i = 0; i < mRequestMap.size(); ++i) {
        if (mRequestMap[i] == &request) {
            mRequestMap[i] = nullptr;
            return true;
        }
    }
    return false;
}

const std::string &ClientContext::getAddress() const {
    return address_;
}

int ClientContext::getPort() const {
    return port_;
}
