//
// Created by user on 12/12/19.
//

#include "metadatatools.hpp"


void to_json(nlohmann::json &j, const TrackMetadata &m) {
    j = nlohmann::json{
        {"uri", m.uri},
        {"artist", m.artist},
        {"album", m.album},
        {"title", m.title},
        {"track", m.track},
        {"length", m.length}};
}

void from_json(const nlohmann::json &j, TrackMetadata &m) {
    if (j.contains("uri"))
        m.uri = j.at("uri");
    if (j.contains("artist"))
        m.artist = j.at("artist");
    if (j.contains("album"))
        m.album = j.at("album");
    if (j.contains("title"))
        m.title = j.at("title");
    if (j.contains("track"))
        m.track = j.at("track");
    if (j.contains("length"))
        m.length = j.at("length");
}
