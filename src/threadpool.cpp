//
// Created by user on 6/13/19.
//

#include "threadpool.hpp"

ThreadPool::ThreadPool() :
    mActive(true)
{

}

ThreadPool::~ThreadPool() {
    mActive = false;

    auto dummy = [](){};

    // join all threads so they can finish any currently running tasks
    for (auto& i : mThreads) {
        queueFn(dummy); // queue a dummy task so it processes
        i.join();
    }
}

bool ThreadPool::queueTask(Task &task) {
    return mTaskQueue.enqueue(std::move(task));
}

void ThreadPool::spawnWorkers(const int &count) {
    for (int i = 0; i < count; ++i) {
        std::thread newThread(&ThreadPool::mWorkerFn, this);
        mThreads.push_back(std::move(newThread));
    }
}

void ThreadPool::mWorkerFn() {
    Task t;
    while(mActive.load()) {
        mTaskQueue.wait_dequeue(t);
        t.mTask();
    }
}

bool ThreadPool::queueFn(std::function<void()> fn) {
    Task t;
    t.mTask = fn;
    return queueTask(t);
}


