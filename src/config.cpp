//
// Created by user on 12/8/19.
//

#include "config.hpp"

#include <boost/property_tree/ini_parser.hpp>

Config::Config(const std::string &file_name) {
    boost::property_tree::read_ini(file_name, mPropTree);
}
