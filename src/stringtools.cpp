//
// Created by user on 12/8/19.
//

#include <algorithm>
#include "stringtools.hpp"

std::string StringTools::toLower(const std::string &in) {
    std::string out = in;
    std::transform(out.begin(), out.end(), out.begin(),
            [](unsigned char c){return std::tolower(c); });
    return out;
}
