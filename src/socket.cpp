//
// Created by user on 6/9/19.
//

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <iostream>
#include "socket.hpp"

Socket::Socket(int port, ThreadPool& pool) :
mPort(port),
mMaxClients(30),
mClientSockets(30),
mThreadPool(pool),
mListenSocket(0)
{
    for (int i = 0; i < mMaxClients; ++i) {
        mClientSockets[i] = 0;
    }

}


void Socket::process() {
    int sd, activity, newSocket, valRead;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    size_t msgLen;

    FD_ZERO(&mReadFDs);

    int maxSD = 0;
    if (mListenSocket != 0) {
        FD_SET(mListenSocket, &mReadFDs);
        maxSD = mListenSocket;
    }

    for (int i = 0; i < mMaxClients; ++i) {
        sd = mClientSockets[i];
        if (sd > 0) {
            FD_SET(sd, &mReadFDs);
        }

        if (sd > maxSD) {
            maxSD = sd;
        }
    }

    // wait for activity on a socket
    activity = select(maxSD + 1, &mReadFDs, NULL, NULL, NULL);

    if ((activity < 0) && (errno != EINTR)) {
        std::cout << "Select error";
        return;
    }

    // check for activity on master socket
    if (FD_ISSET(mListenSocket, &mReadFDs)) {
        if ((newSocket = accept(mListenSocket,
                                (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
        {
            std::cout << "accept failure";
            return;
        }

        for (int i = 0; i < mMaxClients; ++i) {
            if (mClientSockets[i] == 0) {
                mClientSockets[i] = newSocket;
                break;
            }
        }

        std::cout << "new socket: " << newSocket << std::endl;
    }

    for (int i = 0; i < mMaxClients; ++i) {
        sd = mClientSockets[i];

        if (FD_ISSET(sd, &mReadFDs)) {
            // read, check if disconnected
            valRead = read(sd, &msgLen, sizeof(msgLen));
            if (valRead == 0) {

                // client disconnected
                std::cout << "Client disconnected";

                close(sd);
                mClientSockets[i] = 0;

            } else {
                // read message
                std::string msg(msgLen, ' ');
                read(sd, &msg[0], msgLen);

                mThreadPool.queueFn(std::bind(mMsgReceivedFn, *this, i, msg));
                //mMsgReceivedFn(*this, i, msg);
                //std::cout << "Received Message: " << msgLen << " - " << msg << std::endl;
            }
        }
    }

}

void Socket::msgAll(const std::string &msg) {
    for (auto& i : mClientSockets) {
        // skip if invalid connection
        if (i == 0) continue;

        size_t len = msg.length();
        send(i, &len, sizeof(len), 0);
        send(i, &msg[0], len, 0);
    }
}

void Socket::msgClient(const int& client, const std::string& msg) {
    size_t len = msg.length();
    size_t bytesSent = 0;

    send(mClientSockets[client], &len, sizeof(len), 0);

    while(bytesSent < len) {
        int ret = send(mClientSockets[client], &msg[bytesSent], len - bytesSent, 0);

        if (ret < 0) {
            std::cout << "write failure";
            return;
        }

        bytesSent += ret;
    }
}


int Socket::getMaxClients() const {
    return mMaxClients;
}

void Socket::setMsgReceivedFn(std::function<void(Socket &, const int &, const std::string &)> msgReceivedFn) {
    mMsgReceivedFn = msgReceivedFn;
}

bool Socket::initListen(int port) {
    struct sockaddr_in address;
    int opt = 1;

    if ((mListenSocket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        std::cout << "Socket failed";
        return -1;
    }

    if (setsockopt(mListenSocket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        std::cout << "setsockopt failed";
        return -2;
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(mListenSocket, (struct sockaddr *)&address, sizeof(address)) < 0) {
        std::cout << "bind failure";
        return -3;
    }

    if (listen(mListenSocket, 3) < 0) {
        std::cout << "listen failure";
        return -4;
    }
    return false;
}

int Socket::initConnect(const std::string& host, int port) {
    struct sockaddr_in serv_addr;

    //find unused connection
    int client = 0;
    for (; client < mMaxClients; ++client) {
        if (mClientSockets[client] == 0) {
            break;
        }
    }

    if ((mClientSockets[client] = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cout << "socket creation error";
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    if (inet_pton(AF_INET, host.c_str(), &serv_addr.sin_addr) <= 0) {
        std::cout << "invalid address";
        return -2;
    }

    if (connect(mClientSockets[client], (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        std::cout << "connection error";
        return -3;
    }

    return client;
}


