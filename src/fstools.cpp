//
// Created by user on 12/8/19.
//

#include "fstools.hpp"

#include <cstdlib>
#include <boost/filesystem.hpp>

std::string fstools::env(const std::string &v_name) {
    return std::getenv(v_name.c_str());
}

std::string fstools::fs_fallback(const std::vector<std::string> &path_list) {
    for (auto &i : path_list) {
        boost::filesystem::path path(i);
        if (boost::filesystem::exists(path)) {
            return i;
        }
    }
    return "";
}
