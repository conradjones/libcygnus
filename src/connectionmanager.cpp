//
// Created by user on 6/21/19.
//

#include "connectionmanager.hpp"

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <iostream>
#include <algorithm>

#include "clientcontext.hpp"
#include "dataheader.hpp"

std::shared_ptr<ClientContext> ConnectionManager::listenOnPort(int port) {
    auto newCtx = std::make_shared<ClientContext>(*this);
    struct sockaddr_in address = {};
    int opt = 1;

    if ((newCtx->mFD = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        throw std::runtime_error("Socket Failed");
    }

    if (setsockopt(newCtx->mFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        throw std::runtime_error("setsockopt Failed");
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(newCtx->mFD, (struct sockaddr *)&address, sizeof(address)) < 0) {
        throw std::runtime_error("bind Failed");
    }

    if (listen(newCtx->mFD, 3) < 0) {
        throw std::runtime_error("listen Failed");
    }

    newCtx->mOpen = true;
    mListenContexts.push_back(newCtx);
    return newCtx;
}

std::shared_ptr<ClientContext> ConnectionManager::connectTo(const std::string &host, int port) {
    auto newCtx = std::make_shared<ClientContext>(*this);

    struct sockaddr_in serv_addr;
    if ((newCtx->mFD = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        throw std::runtime_error("socket Failed");
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    if (inet_pton(AF_INET, host.c_str(), &serv_addr.sin_addr) <= 0) {
        throw std::runtime_error("Invalid Host");
    }

    if (connect(newCtx->mFD, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        throw std::runtime_error("connect Failed");
    }

    newCtx->mOpen = true;
    newCtx->address_ = host;
    newCtx->port_ = port;
    mClientContexts.push_back(newCtx);
    return newCtx;
}

void ConnectionManager::process(int timeout_ms) {
    int sd, activity, valRead;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    FD_ZERO(&mFDSet);

    int maxFD = 0;
    for (auto& i : mListenContexts) {
        FD_SET(i->mFD, &mFDSet);
        if (i->mFD > maxFD) {
            maxFD = i->mFD;
        }
    }

    for (auto& i : mClientContexts) {
        FD_SET(i->mFD, &mFDSet);
        if (i->mFD > maxFD) {
            maxFD = i->mFD;
        }
    }

    struct timeval *tvp = NULL;
    struct timeval tv = {0,timeout_ms};
    if (timeout_ms >= 0) {
        tvp = &tv;
    }
    activity = select(maxFD + 1, &mFDSet, NULL, NULL, tvp);

    if ((activity < 0) && (errno != EINTR)) {
        throw std::runtime_error("select Failed");
    }

    for (auto& i : mListenContexts) {
        // check if there's any activity
        if (i == nullptr || !FD_ISSET(i->mFD, &mFDSet)) continue;

        auto newCtx = std::make_shared<ClientContext>(*this);
        struct sockaddr_storage addr_storage;
        if ((newCtx->mFD = accept(i->mFD,
                                  (struct sockaddr *)&addr_storage,
                                  (socklen_t *)&addrlen)) < 0)
        {
            throw std::runtime_error("accept Failed");
        }

        char ip_str[INET6_ADDRSTRLEN];
        if (addr_storage.ss_family == AF_INET) {
            auto *s = (struct sockaddr_in *)&addr_storage;
            newCtx->port_ = ntohs(s->sin_port);
            inet_ntop(AF_INET, &s->sin_addr, ip_str, sizeof(ip_str));
            newCtx->address_ = std::string(ip_str);
        }

        newCtx->mOpen = true;
        mClientContexts.push_back(newCtx);
        newConnectionSignal(newCtx);
    }

    for (auto& i : mClientContexts) {
        if (i == nullptr || !FD_ISSET(i->mFD, &mFDSet)) continue;

        DataHeader dh;
        valRead = read(i->mFD, &dh, sizeof(DataHeader));

        if (valRead <= 0) {
            i->mOpen = false;
            close(i->mFD);

            // remove context from contexts
            mClientContexts.erase(std::remove(mClientContexts.begin(),
                    mClientContexts.end(), i), mClientContexts.end());
        } else {
            //read message
            auto data = new char[dh.len];
            valRead = read(i->mFD, data, dh.len);

            //wrap data
            DataWrapper dw(data, dh.len);

            // check if a request is waiting on this response tag
            i->mTagLock.lock();
            if (dh.tag != 0 && i->mRequestMap[dh.tag - 1] != nullptr) {
                i->mRequestMap[dh.tag - 1]->mResponse.set_value(dw);
                i->mRequestMap[dh.tag - 1] = nullptr;
            } else {
                // call global fn
                processMessageSignal(i,dh.tag,dw);
            }
            i->mTagLock.unlock();
        }
    }
}

ConnectionManager::ConnectionManager()
{
    for (int i = 0; i < mRequestMap.size(); ++i) {
        mRequestMap[i] = nullptr;
    }
}
