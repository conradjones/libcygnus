//
// Created by user on 6/21/19.
//

#include "datawrapper.hpp"
#include <string>

DataWrapper::DataWrapper(const std::string& data) :
    mRefCount(new std::atomic<int>(1))
{
    mData = new char[data.size()];
    std::copy(data.begin(), data.end(), mData);
    mSize = data.size();
}

DataWrapper::DataWrapper(const json &data) :
    mRefCount(new std::atomic<int>(1))
{
    std::string serialized = data.dump();
    mData = strdup(serialized.c_str());
    mSize = serialized.size();
}

DataWrapper::~DataWrapper() {
    // if refcount is 1 we're destructing the last instance
    if (mRefCount->load() == 1) {
        free(mData);
        delete mRefCount;
    } else {
        --*mRefCount;
    }
}

DataWrapper::DataWrapper(const DataWrapper &wrapper) :
    mRefCount(wrapper.mRefCount),
    mData(wrapper.mData),
    mSize(wrapper.mSize)
{
    ++*mRefCount;
}

int DataWrapper::getRefCount() const {
    return mRefCount->load();
}

char *DataWrapper::getData() {
    return mData;
}

std::string DataWrapper::getString() {
    return std::string(mData, mSize);
}

DataWrapper::DataWrapper(char *data, size_t size) :
    mRefCount(new std::atomic<int>(1)),
    mData(data),
    mSize(size)
{

}

size_t DataWrapper::size() const {
    return mSize;
}

json DataWrapper::getJSON() {
    return json::parse(getString());
}

