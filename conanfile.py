from conans import ConanFile, CMake, tools


class LibcygnusConan(ConanFile):
    name = "libcygnus"
    scm = {
        "type": "git",
        "subfolder": "libcygnus",
        "url": "auto",
        "revision": "auto"
    }
    version = "0.0.1"
    license = "None"
    author = "mikesbytes mail@mikesbytes.org"
    url = "https://gitlab.com/mikesbytes/libcygnus"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"

    def build_requirements(self):
        self.build_requires("boost/1.71.0")
        self.build_requires("nlohmann_json/3.7.3")
        self.build_requires("concurrentqueue/git")
        self.build_requires("catch2/2.11.0")
        self.build_requires("fmt/6.0.0")


    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="libcygnus")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.hpp", dst="include", keep_path=False)#, src="include")
        self.copy("*hello.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["cygnus"]
        self.cpp_info.includedirs

