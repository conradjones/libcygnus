//
// Created by user on 6/22/19.
//

#include "catch2/catch.hpp"

#include <iostream>
#include "connectionmanager.hpp"
#include "request.hpp"

TEST_CASE( "Connection manager works", "[ConnectionManager]") {
    ConnectionManager cm;
    ConnectionManager cm2;

    auto newConFn = [](std::shared_ptr<ClientContext> ctx) {
        std::string msg = "my string";
        ctx->send(&msg[0], msg.size(), 0);
    };

    auto processMsgFn = [](std::shared_ptr<ClientContext> ctx, uint8_t tag, DataWrapper dw) {
        REQUIRE(tag == 0);
        REQUIRE(dw.getString() == "my string");
    };

    auto processMsgFn2 = [](std::shared_ptr<ClientContext> ctx, uint8_t tag, DataWrapper dw) {
        std::cout << ctx->getAddress() << ":" << ctx->getPort() << std::endl;
        std::string msg = "response";
        ctx->send(&msg[0], msg.size(), tag);
    };

    cm.newConnectionSignal.connect(newConFn);
    cm.processMessageSignal.connect(processMsgFn2);
    cm.listenOnPort(2222);

    cm2.processMessageSignal.connect(processMsgFn);
    auto ctx2 = cm2.connectTo("127.0.0.1", 2222);

    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    cm.process();
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    cm2.process();
    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    Request r(std::string("request"));
    ctx2->processRequest(r);
    cm.process();
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    //std::cout << "test" << std::endl;
    cm2.process();
    auto dw2 = r.getResponse();
    REQUIRE(dw2.getString() == "response");

}

