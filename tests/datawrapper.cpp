//
// Created by user on 6/21/19.
//

#include "catch2/catch.hpp"

#include "datawrapper.hpp"

TEST_CASE( "Refcounting and correctness on DataWrapper", "[DataWrapper]") {
    std::string str("my string");
    DataWrapper dw(str);

    REQUIRE(dw.getRefCount() == 1);
    REQUIRE(dw.getString() == "my string");

    {
        DataWrapper dw2(dw);

        REQUIRE(dw.getRefCount() == 2);
        REQUIRE(dw2.getString() == "my string");

        // check that pointers match
        REQUIRE(dw.getData() == dw2.getData());
    }

    REQUIRE(dw.getRefCount() == 1);
}

TEST_CASE( "JSON capability of DataWrapper", "[DataWrapper]") {
    json j = {{"Hello","World"}};
    DataWrapper dw(j);

    REQUIRE(dw.getString() == j.dump());
    REQUIRE(dw.getJSON() == j);
}
