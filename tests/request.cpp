#include "catch2/catch.hpp"

#include <iostream>
#include "connectionmanager.hpp"
#include "request.hpp"

TEST_CASE( "Request works", "[Request]") {
    ConnectionManager cm;
    ConnectionManager cm2;

    auto processMsgFn = [](std::shared_ptr<ClientContext> ctx, uint8_t tag, DataWrapper dw) {
        if (dw.getString() == "request") {
            std::string msg = "response";
            ctx->send(&msg[0], msg.size(), tag);
        }
    };

    cm.processMessageSignal.connect(processMsgFn);
    cm2.processMessageSignal.connect(processMsgFn);

    cm.listenOnPort(2222);
    auto ctx = cm2.connectTo("127.0.0.1", 2222);

    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    cm.process();

    //std::this_thread::sleep_for(std::chrono::milliseconds(200));
    //cm2.process();
    //std::this_thread::sleep_for(std::chrono::milliseconds(200));

    /*
    Request r(std::string("request"));
    ctx->processRequest(r);
    cm.process();
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    //std::cout << "test" << std::endl;
    cm2.process();
    auto dw2 = r.getResponse();
    REQUIRE(dw2.getString() == "response");

    Request r2(std::string("invalid"));
    ctx->processRequest(r2);

    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    cm.process();

    auto dw3 = r2.getResponse(std::chrono::milliseconds(500));
    REQUIRE(dw3 == std::nullopt);
     */
}

