//
// Created by user on 12/12/19.
//

#ifndef CYGNUS_TRACKMETADATA_HPP
#define CYGNUS_TRACKMETADATA_HPP

#include <string>

struct TrackMetadata{
    std::string uri;
    std::string filename;
    std::string title;
    std::string artist;
    std::string album;
    unsigned track;
    int length;
    int year;
};

#endif //CYGNUS_TRACKMETADATA_HPP
