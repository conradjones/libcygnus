//
// Created by user on 6/21/19.
//

#ifndef CYGNUS_DATAHEADER_HPP
#define CYGNUS_DATAHEADER_HPP

struct __attribute__((__packed__)) DataHeader {
    size_t len;
    uint8_t tag;
};

#endif //CYGNUS_DATAHEADER_HPP
