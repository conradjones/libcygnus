//
// Created by user on 6/21/19.
//

#ifndef CYGNUS_CONNECTIONMANAGER_HPP
#define CYGNUS_CONNECTIONMANAGER_HPP

#include <vector>
#include <memory>
#include <boost/signals2.hpp>
#include <sys/select.h>

#include "threadpool.hpp"
#include "clientcontext.hpp"
#include "request.hpp"

class ConnectionManager {
public:
    ConnectionManager();

    std::shared_ptr<ClientContext> listenOnPort(int port);
    std::shared_ptr<ClientContext> connectTo(const std::string& host, int port);

    void process(int timeout_ms = -1);

    // signals
    boost::signals2::signal<void (std::shared_ptr<ClientContext>)> newConnectionSignal;
    boost::signals2::signal<void (std::shared_ptr<ClientContext>, uint8_t tag, DataWrapper data)> processMessageSignal;

protected:
    std::vector<std::shared_ptr<ClientContext>> mListenContexts;
    std::vector<std::shared_ptr<ClientContext>> mClientContexts;
    std::array<Request*, 255> mRequestMap;

    fd_set mFDSet;
};



#endif //CYGNUS_CONNECTIONMANAGER_HPP
