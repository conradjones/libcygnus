//
// Created by user on 6/21/19.
//

#ifndef CYGNUS_CLIENTCONTEXT_HPP
#define CYGNUS_CLIENTCONTEXT_HPP

#include <stack>
#include <cstdint>
#include <mutex>

#include "request.hpp"

class ConnectionManager;

class ClientContext {
    friend class ConnectionManager;
public:
    ClientContext(ConnectionManager& manager);

    bool processRequest(Request& request);
    bool cancelRequest(Request &request);

    bool send(void *data, size_t length, uint8_t tag = 0);
    bool send(DataWrapper dw, uint8_t tag = 0);

    bool isOpen() const;

    const std::string &getAddress() const;

    int getPort() const;

protected:
    std::stack<uint8_t> mAvailableTags;
    std::array<Request*, 255> mRequestMap;
    int mFD;
    bool mOpen;
    std::string address_;
    int port_;

    std::mutex mTagLock;
    std::mutex mFDLock;

    ConnectionManager &mConnectionManager;
};

typedef std::shared_ptr<ClientContext> ClientCtxPtr ;

#endif //CYGNUS_CLIENTCONTEXT_HPP
