//
// Created by user on 6/21/19.
// A ref counted wrapper with specialization for doing net stuff
//

#ifndef CYGNUS_DATAWRAPPER_HPP
#define CYGNUS_DATAWRAPPER_HPP

#include <atomic>
#include <cstdlib>
#include <string>

#include "nlohmann/json.hpp"

using json = nlohmann::json;

class DataWrapper {
public:
    DataWrapper(const std::string& data);

    DataWrapper(char *data, size_t size);

    DataWrapper(const json &data);

    template<typename T>
    DataWrapper(T data) :
        mRefCount(new std::atomic<int>(1))
    {
        mData = new T(std::move(data));
        mSize = sizeof(T);
    }

    DataWrapper(const DataWrapper &wrapper);

    ~DataWrapper();

    int getRefCount() const;

    char *getData();
    std::string getString();

    json getJSON();

    size_t size() const;

protected:
    std::atomic<int> *mRefCount;
    char *mData;
    size_t mSize;
};


#endif //CYGNUS_DATAWRAPPER_HPP
