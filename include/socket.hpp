//
// Created by user on 6/9/19.
// A wrapper for working with sockets using multiple connections
//

#ifndef CYGNUS_SOCKET_HPP
#define CYGNUS_SOCKET_HPP

#include <unistd.h>
#include <string>
#include <sys/select.h>
#include <vector>
#include <functional>

#include "threadpool.hpp"

class Socket {
public:
    Socket(int port, ThreadPool& pooll);

    bool initListen(int port);
    int initConnect(const std::string& host, int port);

    void msgAll(const std::string& msg);
    void msgClient(const int& client, const std::string& msg);

    void process();

    // getters/setters
    int getMaxClients() const;

    void setMsgReceivedFn(std::function<void(Socket &, const int &, const std::string &)> msgReceivedFn);

protected:
    ThreadPool& mThreadPool;

    int mPort;
    int mSocket;
    int mMaxClients;
    int mListenSocket;

    fd_set mReadFDs;
    std::vector<int> mClientSockets;

    std::function<void(Socket&, const int& client, const std::string& msg)> mMsgReceivedFn;
};


#endif //CYGNUS_SOCKET_HPP
