//
// Created by user on 6/22/19.
//

#ifndef CYGNUS_MESSAGEDISPATCHER_HPP
#define CYGNUS_MESSAGEDISPATCHER_HPP

#include <boost/signals2.hpp>
#include <map>

#include "clientcontext.hpp"
#include "threadpool.hpp"

class MessageDispatcher {
public:
    MessageDispatcher(ThreadPool &pool);

    boost::signals2::signal<void (std::shared_ptr<ClientContext>,
            uint8_t,
            const json&)
            > &getSignal(const std::string& name);

    void processMsg(std::shared_ptr<ClientContext> ctx, uint8_t tag, DataWrapper data);

protected:
    void dispatchDoc(std::shared_ptr<ClientContext> ctx, uint8_t tag, DataWrapper data);

    std::map<
            std::string,
            boost::signals2::signal<void (std::shared_ptr<ClientContext>, uint8_t, const json&)>
            > mDispatchSignals;

    ThreadPool &mThreadPool;
};


#endif //CYGNUS_MESSAGEDISPATCHER_HPP
