//
// Created by user on 6/21/19.
//

#ifndef CYGNUS_REQUEST_HPP
#define CYGNUS_REQUEST_HPP


#include <utility>
#include <future>
#include <chrono>
#include <optional>

#include "datawrapper.hpp"

class ClientContext;

class Request {
    friend class ConnectionManager;
    friend class ClientContext;
public:
    Request(DataWrapper data);
    ~Request();

    DataWrapper getResponse();

    template<class Rep, class Period>
    std::optional<DataWrapper> getResponse(const std::chrono::duration<Rep,Period> &timeout) {
        auto future = mResponse.get_future();
        auto status = future.wait_for(timeout);
        if (status != std::future_status::ready) {
            return std::nullopt;
        }
        return future.get();
    }

protected:
    DataWrapper mReqData;
    std::promise<DataWrapper> mResponse;
    ClientContext *mCTX;

    //std::function<void(ClientCtxPtr, const DataWrapper &)> mCallback;
};


#endif //CYGNUS_REQUEST_HPP
