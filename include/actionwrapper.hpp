//
// Created by user on 12/19/19.
//

#ifndef CYGNUS_ACTIONWRAPPER_HPP
#define CYGNUS_ACTIONWRAPPER_HPP

#include <functional>
#include <utility>
#include <fmt/format.h>
#include "clientcontext.hpp"

template<typename T, typename... Args>
class ActionWrapper {
    typedef std::tuple<std::optional<typename std::decay<Args>::type>...> defaults_tup_type;
public:

    ActionWrapper(
            std::function<T(Args...)> fn,
            const std::string &action_name,
            std::array<std::string, sizeof...(Args)> param_names,
            defaults_tup_type param_defaults
            ) :
            mParamNames(std::move(param_names)),
            mFunction(fn),
            mDefaults(param_defaults)
    {

    }

    ActionWrapper(const ActionWrapper<T, Args...> &wrapper) :
        mParamNames(wrapper.mParamNames),
        mFunction(wrapper.mFunction),
        mDefaults(wrapper.mDefaults)
    {

    }

    void operator() (ClientCtxPtr ctx, uint8_t tag, const json &doc) {
        try {
            // check that doc contains data
            auto it = doc.find("data");
            if (it == doc.end()) {
                throw std::runtime_error("data does not exist");
            }
            auto tup = mUnpackParams(doc);
            T ret = std::apply(mFunction, tup);
            json j = {{"message-type", "response"},
                      {"status",       "success"},
                      {"data",         ret}};
            ctx->send(j, tag);
        } catch (std::exception &e) {
            json j = {{"message-type", "response"},
                      {"status", "error"},
                      {"error-msg",e.what()}};
            ctx->send(j, tag);
        }
    }

private:
    /// Argument tuple type
    typedef std::tuple<typename std::decay<Args>::type...> ttype;

    /// Unpack params from doc into tuple
    ttype mUnpackParams(const json &doc) {
        return mUnpackParams(doc, ttype());
    };

    template<size_t I = 0>
    ttype mUnpackParams(const json &doc, ttype unpacked) {
        if constexpr (I == sizeof...(Args)) {
            return std::move(unpacked);
        } else {
            mParamHandler(std::get<I>(unpacked), std::get<I>(mDefaults), doc, mParamNames[I]);
            return std::move(mUnpackParams<I + 1>(doc, unpacked));
        }
    };

    /// handle individual parameters
    template <typename U>
    void mParamHandler(U &param, std::optional<U> &fallback, const json &doc, const std::string &param_name) {
        auto it = doc["data"].find(param_name);
        if (it == doc["data"].end()) {
            if (fallback.has_value()) {
                param = fallback.value();
            } else {
                throw std::runtime_error(fmt::format("data.{} does not exist", param_name));
            }
        } else {
            param = it.value();
        }
    }

    std::function<T(Args...)> mFunction;
    std::array<std::string, sizeof...(Args)> mParamNames;
    defaults_tup_type mDefaults;
};


template<typename T>
class ActionWrapperNoArgs {
public:

    ActionWrapperNoArgs(
            std::function<T()> fn,
            const std::string &action_name
    ) :
            mFunction(fn)
    {

    }

    ActionWrapperNoArgs(const ActionWrapperNoArgs<T> &wrapper) :
            mFunction(wrapper.mFunction)
    {

    }

    void operator() (ClientCtxPtr ctx, uint8_t tag, const json &doc) {
        try {
            // check that doc contains data
            T ret = mFunction();
            json j = {{"message-type", "response"},
                      {"status",       "success"},
                      {"data",         ret}};
            ctx->send(j, tag);
        } catch (std::exception &e) {
            json j = {{"message-type", "response"},
                      {"status", "error"},
                      {"error-msg",e.what()}};
            ctx->send(j, tag);
        }
    }

private:
    std::function<T()> mFunction;
};

template<typename T>
class ActionWrapperJSONArg {
public:

    ActionWrapperJSONArg(
            std::function<T(const json &)> fn,
            const std::string &action_name
    ) :
            mFunction(fn)
    {

    }

    /*
    ActionWrapperNoArgs(const ActionWrapperNoArgs<T> &wrapper) :
            mFunction(wrapper.mFunction)
    {

    }
     */

    void operator() (ClientCtxPtr ctx, uint8_t tag, const json &doc) {
        try {
            // check that doc contains data
            T ret = mFunction(doc.at("data"));
            json j = {{"message-type", "response"},
                      {"status",       "success"},
                      {"data",         ret}};
            ctx->send(j, tag);
        } catch (std::exception &e) {
            json j = {{"message-type", "response"},
                      {"status", "error"},
                      {"error-msg",e.what()}};
            ctx->send(j, tag);
        }
    }

private:
    std::function<T(const json &)> mFunction;
};

#endif //CYGNUS_ACTIONWRAPPER_HPP
