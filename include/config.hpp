//
// Created by user on 12/8/19.
//

#ifndef CYGNUS_CONFIG_HPP
#define CYGNUS_CONFIG_HPP

#include <string>
#include <boost/property_tree/ptree.hpp>

class Config {
public:
    Config(const std::string &file_name);

    template <typename T>
    T getProperty(const std::string &key, T fallback) const {
        return mPropTree.get(key, fallback);
    }

private:
    boost::property_tree::ptree mPropTree;
};


#endif //CYGNUS_CONFIG_HPP
