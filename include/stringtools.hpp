//
// Created by user on 12/8/19.
//

#ifndef CYGNUS_STRINGTOOLS_HPP
#define CYGNUS_STRINGTOOLS_HPP

#include <string>

namespace StringTools {
    std::string toLower(const std::string &in);
}

#endif //CYGNUS_STRINGTOOLS_HPP
