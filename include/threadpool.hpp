//
// Created by user on 6/13/19.
//

#ifndef CYGNUS_THREADPOOL_HPP
#define CYGNUS_THREADPOOL_HPP

#include <functional>
#include <vector>
#include <blockingconcurrentqueue.h>

class Task {
public:
    std::function<void()> mTask;
};

class ThreadPool {
public:
    ThreadPool();
    ~ThreadPool();

    bool queueTask(Task& task);
    bool queueFn(std::function<void()> fn);
    void spawnWorkers(const int& count);

    
protected:
    void mWorkerFn();

    moodycamel::BlockingConcurrentQueue<Task> mTaskQueue;
    std::vector<std::thread> mThreads;
    std::atomic<bool> mActive;
};


#endif //CYGNUS_THREADPOOL_HPP
